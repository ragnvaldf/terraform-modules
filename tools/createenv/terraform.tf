provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

variable "environment" {}
variable "project" {}
variable "customer" {}
variable "costcenter" {}
variable "maintainer" {}
variable "aws_region" {}
variable "aws_profile" {}

module "state_backend" {
  source      = "git::ssh://git@bitbucket.org/ragnvaldf/terraform-modules//modules/state_backend"
  customer    = "${var.customer}"
  project     = "${var.project}"
  maintainer  = "${var.maintainer}"
  environment = "${var.environment}"
  costcenter  = "${var.costcenter}"
  region      = "${var.aws_region}"

  tags = "${map(
    "ManagedBy", "Terraform",
    "Maintainer", "${var.maintainer}",
    "Costcenter", "${var.costcenter}",
    "Customer", "${var.customer}",
    "Project", "${var.project}",
    "Environment", "${var.environment}"
  )}"
}

output "tfstate" {
  value = "${module.state_backend.tfstate}"
}

output "tflock" {
  value = "${module.state_backend.tflock}"
}
