#!/usr/bin/env bash

tf="terraform.tf"
tfvars="terraform.tfvars"

test -z "$AWS_PROFILE" && {
	echo AWS_PROFILE must be in environment, and accessible in ~/.ssh/credentials >&2
	exit 1
}

if [ "$1"x = "init"x ]
then
	test -r "$tf" || {
		echo Unreadable file $tf >&2 ;
		exit 1 ;
	}

	terraform init || {
		echo Terraform init failed >&2 ;
		echo Ensure terraform is installed. https://www.terraform.io/downloads.html
		exit 1 ;
	}

	exit 0
elif [ "$1"x = "config"x ]
then
	[ ! -f "$tfvars" ] && {
		echo Creating "$tfvars"
		> "$tfvars"
		[ ! -r "$tfvars" ] && { 
			echo Unreadable file: "$tfvars" ;
			exit 1 ;
		}
		echo Set optional defaults. You will be prompted to input what is missing at apply-time if not specified
		for var in environment project customer costcenter maintainer aws_region
		do
			echo -n "$var = "
			read value
			test -z "$value" || echo "$var = \"$value\"" >> "$tfvars"
		done
		echo "aws_profile = \"${AWS_PROFILE}\"" >> "$tfvars"
	}
elif [ "$1"x = "apply"x ]
then
	terraform apply || {
		echo Terraform apply failed >&2
		exit 1
	}
elif [ "$1"x = "create"x ] && [ ! "$2"x = ""x ]
then
	test -d "$2" || test -r "$2" || {
		echo Missing or unreadable directory ${2}. Create it first >&2
		echo mkdir \"${2}\"
		exit 1
	}
	cat << EOF > "${2}/terraform.tf"
terraform {
  backend "s3" {
    encrypt                 = true
    bucket                  = "$(terraform output tfstate)"
    dynamodb_table          = "$(terraform output tflock)"
    region                  = "eu-west-1"
    key                     = "terraform.tfstate"
    shared_credentials_file = ".aws/credentials"
    profile                 = "${AWS_PROFILE}"
  }
}
EOF
	cd "$2" && terraform init || {
		echo Unable to initialize new environment in $2 >&2 ;
		exit 1;
	}
elif [ "$1"x = "clean"x ]
then
	echo Removing files. CTRL+C NOW if you do not wish .terraform to be destroyed.
	test -f "$tfvars" && rm -i "$tfvars"
	test -f "terraform.tfstate" && rm -i "terraform.tfstate"
	test -d ".terraform" && rm -rf ".terraform"
else
	echo "Usage:"
	echo "$(basename $0) init || Initialize terraform. Must be done before each environment"
	echo "$(basename $0) config || Create configuration for new environment"
	echo "$(basename $0) apply || Provision resources for state backend"
	echo "$(basename $0) create <output directory> || Initialize new environment in output directory"
	echo "$(basename $0) clean || Cleans files managed by this script"
	echo This script manages $tfvars .terraform and terraform.tfstate
	echo Run order: init, config, apply, create, clean
	echo If creating multiple environments you can skip config step by not removing $tfvars when cleaning
fi
