# Dependencies
variable "location" {
  description = "Location where resources should be created, or metadata stored"
}

variable "rg_name" {
  description = "Resource group in which all resources are created"
}

variable "app_sp_name" {
  description = "Name of app service plan"
}

variable "app_sp_id" {
  description = "Id of app service plan"
}

variable "blob_sas_url" {
  description = "Sas token for storage container"
}

# Configuration
variable "app_name" {}

variable "tags" {
  type = "map"
}

variable "appsettings" {
  type = "map"
}

variable "connectionstrings" {
  type = "map"
}

variable "slotconfignames" {
  type = "map"
}

variable "logs" {
  type = "map"
}

variable "web" {
  type = "map"
}
