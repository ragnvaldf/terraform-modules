resource "azurerm_template_deployment" "app_insights" {
  name                = "app_insights-${var.app_name}"
  resource_group_name = "${var.rg_name}"
  deployment_mode     = "Incremental"

  lifecycle {
    prevent_destroy = true
  }

  template_body = "${file("${path.module}/app_insights.json")}"

  parameters {
    "name"     = "${var.app_name}"
    "location" = "${var.location}"
  }
}

resource "azurerm_app_service" "app_service" {
  name                = "${var.app_name}"
  location            = "${var.location}"
  resource_group_name = "${var.rg_name}"
  app_service_plan_id = "${var.app_sp_id}"

  lifecycle {
    prevent_destroy = true
  }

  https_only = "true"

  tags = "${var.tags}"

  depends_on = ["azurerm_template_deployment.app_insights"]
}

resource "azurerm_template_deployment" "app_insight_extension" {
  name                = "app_insight_extension-${var.app_name}"
  resource_group_name = "${var.rg_name}"
  deployment_mode     = "Incremental"

  lifecycle {
    prevent_destroy = false
  }

  template_body = "${file("${path.module}/siteextensions.json")}"

  parameters {
    "name"     = "${azurerm_app_service.app_service.name}"
    "location" = "${azurerm_app_service.app_service.location}"
  }

  depends_on = ["module.render_appsettings"]
}

module "render_appsettings" {
  source   = "../arm_deploy/"
  rg_name  = "${azurerm_app_service.app_service.resource_group_name}"
  location = "${azurerm_app_service.app_service.location}"

  name = "${azurerm_app_service.app_service.name}/appsettings"

  properties = "${merge(map(
    "properties.value.SCM_SITEEXTENSIONS_FEED_URL", "http://www.siteextensions.net/api/v2/",
    "properties.value.APPINSIGHTS_INSTRUMENTATIONKEY", "${azurerm_template_deployment.app_insights.outputs["instrumentation_key"]}",
    "properties.value.APPINSIGHTS_PORTALINFO", "ASP.NET",
    "properties.value.APPINSIGHTS_PROFILERFEATURE_VERSION", "disabled",
    "properties.value.DIAGNOSTICS_AZUREBLOBRETENTIONINDAYS", "30",
    "properties.value.DIAGNOSTICS_AZUREBLOBCONTAINERSASURL", "${var.blob_sas_url}"
    ), var.appsettings)}"

  template_file = "${path.module}/siteconfig.json"
  render_file   = "app_service_appsettings_rendered.json"
}

module "render_connectionstrings" {
  source   = "../arm_deploy/"
  rg_name  = "${azurerm_app_service.app_service.resource_group_name}"
  location = "${azurerm_app_service.app_service.location}"

  name          = "${azurerm_app_service.app_service.name}/connectionstrings"
  properties    = "${var.connectionstrings}"
  template_file = "${path.module}/siteconfig.json"
  render_file   = "app_service_connectionstrings_rendered.json"
}

module "render_slotconfignames" {
  source   = "../arm_deploy/"
  rg_name  = "${azurerm_app_service.app_service.resource_group_name}"
  location = "${azurerm_app_service.app_service.location}"

  name          = "${azurerm_app_service.app_service.name}/slotconfignames"
  properties    = "${var.slotconfignames}"
  template_file = "${path.module}/siteconfig.json"
  render_file   = "app_service_slotconfignames_rendered.json"
}

module "render_logs" {
  source   = "../arm_deploy/"
  rg_name  = "${azurerm_app_service.app_service.resource_group_name}"
  location = "${azurerm_app_service.app_service.location}"

  name          = "${azurerm_app_service.app_service.name}/logs"
  properties    = "${var.logs}"
  template_file = "${path.module}/siteconfig.json"
  render_file   = "app_service_logs_rendered.json"
}

module "render_web" {
  source   = "../arm_deploy/"
  rg_name  = "${azurerm_app_service.app_service.resource_group_name}"
  location = "${azurerm_app_service.app_service.location}"

  name          = "${azurerm_app_service.app_service.name}/web"
  properties    = "${var.web}"
  template_file = "${path.module}/siteconfig.json"
  render_file   = "app_service_web_rendered.json"
}
