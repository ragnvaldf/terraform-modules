locals {
  naming_convention = {
    "resource_group"       = "${var.project}-rg"
    "app_service"          = "${var.environment == "prod" ? "${var.project}" : "${var.project}-${var.environment}"}"
    "sql_server"           = "${var.project}-sql-${var.environment}"
    "database"             = "${var.project}-db-${var.environment}"
    "service_plan"         = "${var.project}-sp"
    "application_insights" = "${var.project}-ai-${var.environment}"
    "storage_account"      = "${lower("${var.project}${var.environment}")}"
    "storage_container"    = "${var.project}-sc-${var.environment}"
    "tfstate"              = "${var.project}-tfstate-${var.environment}"
    "tflock"               = "${var.project}-tflock-${var.environment}"
  }
}
