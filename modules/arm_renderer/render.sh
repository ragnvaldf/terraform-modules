#!/bin/bash

#set -e

Exit () {
  echo "Error: $1" >&2
  exit 1
}

unQuote () {
  temp="${1%\"}"
  temp="${temp#\"}"
  echo "$temp"
}

parameter_file="$1" # First argument should be existing json file containing the parameters node from an ARM template parameter file
test -r "$parameter_file" || Exit "$parameter_file not readable"

render_file="$2" # Second parameter is where the rendered template should be stored. It will be TRUNCATED!
test -w "$render_file" || touch "$render_file" || Exit "$render_file not writable"

jq="$(command -v jq)" # Handy tool to simplify json processing
test -x "$jq" || Exit "jq not executable. Install using apt install jq or download from https://stedolan.github.io/jq"

cat="$(command -v cat)"
test -x "$cat" || Exit "cat not executable."

cut="$(command -v cut)"
test -x "$cut" || Exit "cut not executable."

input="$($cat)" # Read stdin until EOF. Terraform supplies simple key value pairs as json

parameters="$($cat "$parameter_file" | $jq -r .)" # Begin processing with parameter_file as base template
test $? = 0 || Exit "jq failed reading $parameter_file"

while read -r pair; do # For each pair
  key="$(echo $pair | $cut -d',' -f1)"
  key=".$(unQuote "$key")"
  test -n "$key" || Exit "Invalid key from stdin: ${key}"

  value="$(echo $pair | $cut -d',' -f2-)"
  test -n "$value" || Exit "Invalid value from stdin: ${value}"
  
  kvpair="${key}=${value}"
  #echo >&2 "key = $key"
  #echo >&2 "value = $value"

  output="$(echo $parameters | $jq "$kvpair" -)" # Make input field identified by key, contain value. Creates if missing.
  test $? = 0 || Exit "jq failed at $kvpair -- $pair"
  test -n "$output" || Exit "jq gave empty output in key=$key"
  parameters="$output"
done< <( echo $input | $jq -r 'to_entries[] | [.key, .value] | @csv' - ) # Flattens input from terraform into csv keypairs

echo $parameters | $jq . - > "$render_file"
test $? = 0 && test -r "$render_file" && echo "{ \"file\": \"$render_file\" }" && exit 0
Exit "$render_file was not rendered"
