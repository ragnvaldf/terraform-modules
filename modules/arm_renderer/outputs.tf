output "file" {
  value       = "${data.external.arm_renderer.result["file"]}"
  description = "Use this output instead of input variable, output_file to ensure dependency graph is built correctly by terraform"
}
