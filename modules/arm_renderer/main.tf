data "external" "arm_renderer" {
  program = ["bash", "${path.module}/render.sh", "${var.input_file}", "${var.output_file}"]
  query   = "${var.keyvalues}"
}
