variable "keyvalues" {
  description = "Map of key value pairs to merge with input_file to produce new file output_file"
  type        = "map"
}

variable "input_file" {}

variable "output_file" {}
