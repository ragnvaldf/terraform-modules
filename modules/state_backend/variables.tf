variable "region" {}

variable "customer" {}

variable "project" {}

variable "environment" {}

variable "costcenter" {}

variable "maintainer" {}

variable "tags" {
  type = "map"
}
