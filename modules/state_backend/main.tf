locals {
  tfstate = "${lookup(module.naming.convention, "tfstate")}"
  tflock  = "${lookup(module.naming.convention, "tflock")}"
  tags    = "${var.tags}"
}

module "naming" {
  source      = "../naming_convention"
  environment = "${var.environment}"
  project     = "${var.project}"
}

resource "aws_s3_bucket" "tfstate" {
  bucket = "${local.tfstate}"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = "${merge(
     local.tags,
     map(
       "Name", "${local.tfstate}"
     )
   )}"

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_dynamodb_table" "tflock" {
  name           = "${local.tflock}"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = "${merge(
     local.tags,
     map(
       "Name", "${local.tflock}"
     )
   )}"

  lifecycle {
    prevent_destroy = true
  }
}
