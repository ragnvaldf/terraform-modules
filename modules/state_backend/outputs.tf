output "tfstate" {
  description = "S3 bucket name"
  value       = "${aws_s3_bucket.tfstate.bucket}"
}

output "tflock" {
  description = "DynamoDB table name"
  value       = "${aws_dynamodb_table.tflock.name}"
}
