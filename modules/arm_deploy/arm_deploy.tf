data "template_file" "template" {
  template = "${file(var.template_file)}"
}

module "renderer" {
  source      = "../arm_renderer/"
  input_file  = "${path.module}/emptyParameters.json"
  output_file = "${var.render_file}"

  keyvalues = "${merge(map(
    "name.value", "${var.name}",
    "location.value", "${var.location}",
  ), var.properties)}"
}

data "template_file" "params" {
  template = "${file("${module.renderer.file}")}"
}

resource "azurerm_template_deployment" "arm_deploy" {
  name                = "arm_deploy_${replace("${var.name}", "/", "-")}"
  resource_group_name = "${var.rg_name}"
  deployment_mode     = "Incremental"

  lifecycle {
    prevent_destroy = false
  }

  template_body   = "${data.template_file.template.rendered}"
  parameters_body = "${data.template_file.params.rendered}"
}
