variable "name" {}

variable "rg_name" {}

variable "location" {}

variable "properties" {
  type = "map"
}

variable "render_file" {}

variable "template_file" {}
